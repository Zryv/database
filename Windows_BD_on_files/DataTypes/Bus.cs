﻿namespace Windows_BD_on_files.DataTypes
{
    public class Bus : Transport
    {
        public int NumbersOfPassengers { get; set; }

        public Bus(string model, string name, double engineCapacity, int numbersOfPassengers)
        {
            Model = model;
            Name = name;
            EngineCapacity = engineCapacity;
            NumbersOfPassengers = numbersOfPassengers;
        }
    }
}
