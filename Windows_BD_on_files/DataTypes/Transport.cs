﻿namespace Windows_BD_on_files.DataTypes
{
    public abstract class Transport
    {
        public int ID { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public double EngineCapacity { get; set; }
    }
}