﻿namespace Windows_BD_on_files.DataTypes
{
    public class Car : Transport
    {
        public double MaxSpeed { get; set; }

        public Car(string model, string name, double engineCapacity, double maxSpeed)
        {
            Model = model;
            Name = name;
            EngineCapacity = engineCapacity;
            MaxSpeed = maxSpeed;
        }
    }
}
