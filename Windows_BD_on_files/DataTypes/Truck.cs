﻿namespace Windows_BD_on_files.DataTypes
{
    public class Truck : Transport
    {
        public double Weight { get; set; }

        public Truck(string model, string name, double engineCapacity, double weight)
        {
            Model = model;
            Name = name;
            EngineCapacity = engineCapacity;
            Weight = weight;
        }
    }
}
