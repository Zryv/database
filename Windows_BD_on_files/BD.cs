﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Windows_BD_on_files
{
    public class DB
    {
        public string Path { get; private set; }
        public int LastID { get; private set; }
        public int Count => _table.Count;

        private List<Transport> _table;

        public DB(string path = null)
        {
            _table = new List<Transport>();
            LastID = 0;

            Path = path;
            if (string.IsNullOrEmpty(path))
            {
                Path = $"DefaultFileOfSerialization_{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json";
            }
            else
            {
                Load();
            }
        }

        public Transport[] ToArray() => _table.ToArray();

        public Transport Find(int id)
        {
            foreach (var transport in _table)
            {
                if (id == transport.ID)
                {
                    return transport;
                }
            }
            return null;
        }
        public bool Contains(Transport transport) => _table.Contains(transport);

        public void Add(Transport transport)
        {
            transport.ID = LastID++;
            _table.Add(transport);
        }

        public void Delete(int id)
        {
            foreach (var transport in _table)
            {
                if (id == transport.ID)
                {
                    Delete(transport);
                    break;
                }
            }
        }
        public void Delete(Transport deleteTransport) => _table.Remove(deleteTransport);

        public bool Save()
        {
            try
            {
                using (var stream = new FileStream(Path, FileMode.Create))
                using (var writer = new StreamWriter(stream))
                {
                    var container = new SerializeBox(_table, LastID);
                    writer.Write(JsonConvert.SerializeObject(container, CreateSettings()));

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Load()
        {
            if (string.IsNullOrEmpty(Path))
            {
                return false;
            }

            try
            {
                using (var stream = new FileStream(Path, FileMode.OpenOrCreate))
                using (var reader = new StreamReader(stream))
                {
                    var container = JsonConvert.DeserializeObject<SerializeBox>(reader.ReadToEnd(), CreateSettings());
                    _table = container.LstTransport;
                    LastID = container.LastID;

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private JsonSerializerSettings CreateSettings()
        {
            var result = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };

            return result;
        }

        private class SerializeBox
        {
            public List<Transport> LstTransport { get; set; }
            public int LastID { get; set; }

            public SerializeBox(List<Transport> lstTransport, int lastID)
            {
                LstTransport = lstTransport;
                LastID = lastID;
            }
        }
    }
}
