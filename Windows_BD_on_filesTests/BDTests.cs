﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using Windows_BD_on_files.DataTypes;

namespace Windows_BD_on_files.Tests
{
    [TestClass]
    public class BDTests
    {
        private DB _database;

        [TestInitialize]
        public void Initialization()
        {
            _database = new DB();
        }

        [TestMethod]
        public void CreateDatabaseCheck()
        {
            var path = "1.json";
            File.Delete(path);

            var database = new DB(path);
            Assert.AreEqual(0, database.Count);

            AddNewItem(database);
            AddNewItem(database);
            database.Save();

            database = new DB(path);
            Assert.AreEqual(2, database.Count);
        }

        [TestMethod]
        public void GetTransportAtCheck()
        {
            var transport = GetNewTransportCar();
            _database.Add(transport);
            var localTransport = _database.Find(transport.ID);
            Assert.AreEqual(transport, localTransport);
        }

        [TestMethod]
        public void AddTransportAndCheckIncrementCount()
        {
            var lastID = _database.LastID;
            _database.Add(GetNewTransportCar());
            Assert.AreNotEqual(lastID, _database.LastID);
        }

        [TestMethod]
        public void AddTransportAndSaveLoadWithPathNull()
        {
            for (int i = 0; i < 5; i++)
            {
                AddNewItem(_database);
            }
            var newTransport = AddNewItem(_database);
            var id = newTransport.ID;
            var transport = _database.Find(id);
            Assert.AreEqual(transport, newTransport);
            Assert.AreEqual(6, _database.Count);

            _database.Save();
            var path = _database.Path;
            _database = new DB(path);
            transport = _database.Find(id);
            Assert.AreEqual(6, _database.Count);
            Assert.AreEqual(transport.ToString(), newTransport.ToString());

            //Check on null path 
            _database = new DB();
            _database.Load();
        }

        [TestMethod]
        public void DeleteTransportRight()
        {
            var id = 0;
            for (int i = 0; i < 5; i++)
            {
                AddNewItem(_database);
            }
            var arrayBase = _database.ToArray();
            foreach (var transport in arrayBase)
            {
                id = transport.ID;
                _database.Delete(id);
                Assert.IsFalse(_database.Contains(transport));
            }
            _database.Delete(id);

            //Check on null base
            _database = new DB();
            _database.Delete(id);
        }

        [TestMethod]
        public void CheckWorkLastIndex()
        {
            var path = "2.json";
            _database = new DB(path);
            var max = 0;
            for (int i = 0; i < 5; i++)
            {
                max = Math.Max(max, AddNewItem(_database).ID);
            }
            for (int i = max, j = max - 2; i > j; i--)
            {
                _database.Delete(i);
            }
            _database.Save();
            _database = new DB(path);
            var transport = AddNewItem(_database);
            Assert.IsNull(_database.Find(max));
            Assert.IsNotNull(_database.Find(transport.ID));
            Assert.IsTrue(max < transport.ID);
        }

        private string[] _modelsLine = { "BMW", "Aston Martin", "Mersedes", "VAZ", "Ford" };
        private string[] _names = { "X2", "bublle", "Kalina", "Repulsor", "Sayber", "X3", "TY", "Garrus" };
        private Random _rand = new Random();
        private string NewModel => _modelsLine[_rand.Next() % 5];
        private string NewName => _names[_rand.Next() % 8];
        private double NewEngineCapacity => (5 * _rand.NextDouble());
        private double NewWeight => 10000 + _rand.Next() % 10000;
        private int NewNumbersOfPassengers => 16 + _rand.Next() % 40;
        private double NewMaxSpeed => 160 + 100 * _rand.NextDouble();

        public Transport GetNewTransportCar() => new Car(NewModel, NewName, NewEngineCapacity, NewMaxSpeed);
        public Transport GetNewTransportBus() => new Bus(NewModel, NewName, NewEngineCapacity, NewNumbersOfPassengers);
        public Transport GetNewTransportTruck() => new Truck(NewModel, NewName, NewEngineCapacity, NewWeight);
        public Transport AddNewItem(DB database)
        {
            var transport = GetNewTransportCar();
            database.Add(transport);

            return transport;
        }

    }
}